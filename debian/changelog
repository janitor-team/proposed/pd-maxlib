pd-maxlib (1.5.5-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Make sure that CPPFLAGS are applied
  * Removed Hans-Christoph Steiner from Uploaders as he requested
  * Add salsa-ci configuration
  * Remove obsolete file d/source/local-options
  * Apply "warp-and-sort -ast"
  * Bump dh-compat to 13
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 04 Sep 2022 14:01:33 +0200

pd-maxlib (1.5.5-3) unstable; urgency=medium

  * Added patch for 64bit-safe table-access (Closes: #792723)
  * Don't require "root" for building
  * Bumped standards to 4.2.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 30 Sep 2018 20:51:56 +0200

pd-maxlib (1.5.5-2) unstable; urgency=medium

  * Patch to fix format-security errors
  * Simplified & unified d/rules
    * Bumped dh compat to 11
    * Enabled hardening
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
    * Added myself to Uploaders
  * Switched URLs to https://
  * Updated d/copyright(_hints)
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:22:05 +0100

pd-maxlib (1.5.5-1) unstable; urgency=low

  * Depends: puredata-core | pd, so the depends is not only on a virtual package
  * updated to copyright-format/1.0
  * Standards-Version: 3.9.4
  * Imported Upstream version 1.5.5
  * add patch to force .pd_linux as the binary file extension

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 17 Jan 2013 22:34:30 -0500

pd-maxlib (1.5.4-1) unstable; urgency=low

  * actually fix Hurd/kFreeBSD via upstream Makefile update
  * removed patches since they have been replaced by upstream updates

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 28 Dec 2011 21:25:12 -0500

pd-maxlib (1.5.3-2) unstable; urgency=low

  [ IOhannes m zmölnig ]
  * Makefile-fixes for non-linux (Closes: #609431)
  * Removed quilt dependency; we use source 3.0 (quilt)
  * Added Vcs stanzas

  [ Hans-Christoph Steiner ]
  * bumped standards version to 3.9.2
  * updated Build-Depends to use puredata-dev (Closes: #629716)

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 10 Jun 2011 14:36:44 -0400

pd-maxlib (1.5.3-1) unstable; urgency=low

  * Initial release (Closes: #591825)

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 10 Nov 2010 15:14:39 -0500
